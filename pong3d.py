import pygame
import sys
import time
import pygame
from obsticles import Obsticle

room = {
	'width': 1200,
	'length': 1200,
	'height': 800
}

net = Obsticle(0, room['height']-100, room['length']/2, room['width'], 50, room['length'])

pygame.init()
pygame.font.init()
myfont = pygame.font.SysFont('Comic Sans MS', 20)

screen = pygame.display.set_mode([1200, 800])
pygame.display.set_caption('PONG')

dt = 0.005
dy = 0
gravity = -0.2
airFriction = 0.995

xCoord = 100
yCoord = 100
zCoord = 100

xSpeed = 0
ySpeed = 0
zSpeed = 0

while True:
	screen.fill((255, 255, 255))
	if xCoord + 50 >= room['width'] or xCoord - 50 <= 0:
		xSpeed *= -1
	if zCoord + 50 >= room['length'] or zCoord - 50 <= 0:
		zSpeed *= -1
	if yCoord + 50 >= room['height']:
		ySpeed *= -1

	#collision with net left and right
	zInRange1 = (abs(net.z - (zCoord + 50)) <= 5) or (abs(net.z - (zCoord - 50)) <= 5)
	yInRange1 = (room['height']  - (yCoord + 50)) <= net.height
	xInRange1 = True
	if zInRange1 and yInRange1 and xInRange1:
		zSpeed *= -1

	#collision with the net on top
	zInRange = (zCoord + 50 >= net.z - 30) and (zCoord - 50 <= net.z + 30)
	yInRange = net.y - (yCoord + 50) <= 10#net.y - 10 <= (yCoord + 50) <= net.y + 10
	xInRange = True

	if zInRange and yInRange and xInRange:
		#if zInRange and yCoord >= net.y:
		#	yCoord = net.y - 50
		ySpeed *= -1


	#******

	ySpeed -= gravity

	xCoord += xSpeed
	yCoord += ySpeed
	zCoord += zSpeed

	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			sys.exit()
		elif event.type == pygame.KEYDOWN:
			if event.key == pygame.K_w:
				ySpeed = (ySpeed + 3) * -1
			elif event.key == pygame.K_s:
				yCoord *= -1
			elif event.key == pygame.K_LEFT:
				xSpeed -= 3
			elif event.key == pygame.K_RIGHT:
				xSpeed += 3
			elif event.key == pygame.K_UP:
				zSpeed += 3
			elif event.key == pygame.K_DOWN:
				zSpeed -= 3

	xSpeed *= airFriction
	ySpeed *= airFriction
	zSpeed *= airFriction

	if yCoord + 50 > room['height'] + 1:
		yCoord = room['height'] - 50

	textsurface = myfont.render("x: %d y: %d z: %d" % (xCoord, yCoord, zCoord), False, (0, 0, 0)) #shows score

	screen.blit(textsurface,(0,0))

	pygame.draw.circle(screen, (0, 0, 0), [round(zCoord), round(yCoord)], 50)

	pygame.draw.rect(screen, (0, 0, 0), [net.z, net.y, 3, net.width], 2)

	pygame.display.flip()


	time.sleep(dt)






