from vector import Vector


class Controller:
    def __init__(self, controller_id, game_code):
        self.id = controller_id
        self.code = game_code

        # Meters/second^2
        self.acceleration = Vector(0, 0, 0)

        # Degrees and degrees/seconds
        self.orientation = Vector(0, 0, 0)
        self.orientation_rate = Vector(0, 0, 0)
