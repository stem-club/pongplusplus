import random

from flask import Flask
from flask_socketio import SocketIO, join_room, emit

from controller import Controller
from game import Game

app = Flask(__name__)
socketio = SocketIO(app, cors_allowed_origins=None)
ROOMS = {}


def create_id(length=10, code_chars="abcdefghijklmnopqrstuvwxyz0123456789"):
    result = ""
    for i in range(length):
        result += random.choice(code_chars)
    return result


@socketio.on("create")
def on_create():
    """Controller client wants to create a new game"""
    game = Game()
    ROOMS[game.get_code()] = game
    join_room(game.get_code())

    emit("game_code", {
        "code": game.get_code()
    }, broadcast=True)


@socketio.on("connect_controller")
def connect_controller(data):
    """A controller asks to be connected. It has the game code and a unique controller ID."""
    game_code = data["code"]
    controller_id = data["id"]
    join_room(game_code)

    game = ROOMS[data["code"]]

    controller = Controller(controller_id, game_code)
    game.add_controller(controller)

    emit("controller_connected", {
        "id": controller_id
    }, broadcast=True)


@socketio.on("start_game")
def start_game(data):
    """Display client said to start, so tell controls to start."""
    game_code = data["code"]
    join_room(game_code)

    emit("start_game", broadcast=True)


@socketio.on("controller_update")
def controller_update(data):
    """A controller sends the updated accel and gyro."""
    game_code = data["code"]
    controller_id = data["id"]
    join_room(game_code)

    print(data)

    game: Game = ROOMS[data["code"]]

    accel = data["acceleration"]
    orient = data["orientation"]
    orient_r = data["orientation_rate"]

    game.controllers[controller_id].acceleration.x = accel[0]
    game.controllers[controller_id].acceleration.y = accel[1]
    game.controllers[controller_id].acceleration.z = accel[2]

    game.controllers[controller_id].orientation.x = orient[0]
    game.controllers[controller_id].orientation.y = orient[1]
    game.controllers[controller_id].orientation.z = orient[2]

    game.controllers[controller_id].orientation_rate.x = orient_r[0]
    game.controllers[controller_id].orientation_rate.y = orient_r[1]
    game.controllers[controller_id].orientation_rate.z = orient_r[2]

    controller_thing = [{
        "accel": {
            "x": game.controllers[controller].acceleration.x,
            "y": game.controllers[controller].acceleration.y,
            "z": game.controllers[controller].acceleration.z
        },
        "orientation": {
            "x": game.controllers[controller].orientation.x,
            "y": game.controllers[controller].orientation.y,
            "z": game.controllers[controller].orientation.z
        },
        "orientation_rate": {
            "x": game.controllers[controller].orientation_rate.x,
            "y": game.controllers[controller].orientation_rate.y,
            "z": game.controllers[controller].orientation_rate.z
        }
    } for controller in game.controllers]
    emit("controller_update", controller_thing, broadcast=True)


if __name__ == "__main__":
    socketio.run(app, host="0.0.0.0", debug=True)
