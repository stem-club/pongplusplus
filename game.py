import random
from typing import Dict

from controller import Controller

code_chars = "abcdefghijklmnopqrstuvwxyz0123456789"


class Game:
    def __init__(self):
        self.code = ""
        for i in range(3):
            self.code += random.choice(code_chars)

        self.controllers: Dict[str, Controller] = {}

    def get_code(self):
        return self.code

    def add_controller(self, controller: Controller):
        self.controllers[controller.id] = controller
